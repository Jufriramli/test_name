# Please write a code to detect whether each data point 
# in the file has 80 to 120ms difference. If not, count 
# the total number of data point inconsistencies (Classified as 
# “Less than 50ms”, “Greater than 500ms”, “Out of Spec“ 
# (50 to 80 & 120 to 500).

# less than 0.05
# 0.05 to 0.08 out of spec
# 0.08 to 0.12 ok
# 0.12 to 0.5 out of spec
# more than 0.5 

# 1 ms = 0.001 sec
# 80 ms = 0.08 sec
# 120 ms = 0.12 sec

from data.load_vessel_data import load_vessel_data
import numpy as np

def time_diff_tag(time_diff_sec):
    if time_diff_sec < 0.05:
        return "Less than 50ms"
    elif time_diff_sec > 0.5:
        return "Greater than 500ms"
    elif time_diff_sec < 0.08 or time_diff_sec > 0.12:
        return "Out of Spec"
    else:
        return "Valid"

def check_inconsistent_datapoint(path):

    col_inc=["time_diff_tag","time_diff_sec" ]
    df_day_combined = load_vessel_data(path)
        
    df_day_combined["time_diff_sec"]=df_day_combined.index.to_series().diff() / np.timedelta64(1, 's')
    df_day_combined["time_diff_tag"]=df_day_combined["time_diff_sec"].map(time_diff_tag)

    if (df_day_combined[df_day_combined["time_diff_tag"] != "Valid"].empty):
        print("No issue")
    else:
        values = df_day_combined[df_day_combined["time_diff_tag"] != "Valid"]["time_diff_tag"].value_counts().keys().tolist()
        counts = df_day_combined[df_day_combined["time_diff_tag"] != "Valid"]["time_diff_tag"].value_counts().tolist()
        for v, c in zip(values, counts) :
            print('{}: {}'.format(v, c))
            # bins_list = [-0.05, 0.06, 0.08, 0.12, 0.14, 0.16, 0.18, 0.2, 0.3]
            # df_day_combined[["time_diff_sec"]][df_day_combined["time_diff_tag"]==v].hist(bins=bins_list)
        return df_day_combined[col_inc], df_day_combined[col_inc][df_day_combined["time_diff_tag"] != "Valid"]

path = "/Users/jufri/Dravam Drive/TS_datavis/KC_AWS/2021/09/11/*.csv" 
df_day_combined, inconsistent_datapoint = check_inconsistent_datapoint(path)