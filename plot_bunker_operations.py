# %%

import glob
import pandas as pd
import numpy as np

path = "exported_events_raw_data/Loading/*.csv" 
path = "exported_events_raw_data/Unloading/*.csv" 

all_filenames = [i for i in glob.glob(path)]
all_filenames = sorted(all_filenames)
all_filenames = all_filenames[:]
for fname in glob.glob(path):
    print(fname)

    # operation is extracted from last portion of filename (Loading, Unloading, Outlier)
    operation = fname.split("_")[-1].replace(".csv","")

    header_list = [
        "time", "date_year", "date_month", "date_day", 
        "time_hr", "time_min", "time_sec", "time_Msec", 
        "pt_0001_load_value", "tt_0001_load_value", # pressure, temp
        "dp_0001_load_value", "dp_0001_unload_value", # diff pressure low 0-2000, high 0-5000
        "dp_0002_load_value", "dp_0002_unload_value",
        ]

    variable_list=[
        "pt_0001_load_value", "tt_0001_load_value", 
        "dp_0001_load_value", "dp_0001_unload_value",
        "dp_0002_load_value", "dp_0002_unload_value"
        ]

    operation_df = pd.read_csv(fname, names=["time"]+variable_list, header=0,  index_col=False)

    operation_df=operation_df[["time"]+variable_list]

    operation_df['time'] = pd.to_datetime(operation_df['time'], format='%Y-%m-%d %H:%M:%S.%f')

    # sort combined data by time
    operation_df=operation_df.sort_values(by='time', ascending=True)

    operation_df[variable_list].plot(figsize=(15,6)).get_figure().savefig("export_bunker_plot/"+operation+"_"+fname.split("/")[2][:-4]+".pdf")

