#%%

import glob
import pandas as pd
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt

import os

# update the date_folder
folder = "KC_AWS/"
date_folder = "2021/09/01/"
files = "*.csv"

path = r"/Users/vivek/Google Drive/Dravam_Vivek/Data/KC_AWS/*.csv"
path = r"/Users/jufri/Google Drive/Dravam/TS_datavis/KC_AWS/*.csv"
path = r"/Users/jufri/Google Drive/Dravam/TS_datavis/KC_AWS/2021/09/05/*.csv"
path = folder+date_folder+files

header_list = [
   "time", "date_year", "date_month", "date_day", 
   "time_hr", "time_min", "time_sec", "time_Msec", 
   "pt_0001_load_value", "tt_0001_load_value", # pressure, temp
   "dp_0001_load_value", "dp_0001_unload_value", # diff pressure low 0-2000, high 0-5000
   "dp_0002_load_value", "dp_0002_unload_value",
   ]

variable_list=[
   "pt_0001_load_value", "tt_0001_load_value", 
   "dp_0001_load_value", "dp_0001_unload_value",
   "dp_0002_load_value", "dp_0002_unload_value"
]

# for fname in glob.glob(path):
#    print(fname)
# extension = 'csv'
# all_filenames = [i for i in glob.glob('*.{}'.format(extension))]
# print(all_filenames)

all_filenames = [i for i in glob.glob(path)]
all_filenames = sorted(all_filenames)
all_filenames = all_filenames[:]
# for fname in glob.glob(path):
#    print(fname)

# #combine all files in the list
df_day_combined = pd.concat([pd.read_csv(f, names=header_list) for f in all_filenames ])



#%%

df_day_combined=df_day_combined[["time"]+variable_list]
df_day_combined['time'] = pd.to_datetime(df_day_combined['time'], format='%Y-%m-%d %H:%M:%S.%f')
df_day_combined.info()

#%%

# sort combined data by time
df_day_combined=df_day_combined.sort_values(by='time', ascending=True)

# %%
# downsample data

# 24 hours (10 reading a sec) - 864001
# 15 min - 900
# 15 sec - 150
# 10 sec - 100
# 1 min- 600
# 24 hours (1 reading per 15 sec) - 5760

# always do mva, then downsample

df_day_combined_downsample = df_day_combined.iloc[::600]
df_day_combined_downsample.plot(x='time',subplots=True, sharex=True, figsize=(15,10))

# %%

# 1 sec - 10 datapoints
# 3000 points = 300 secs = 5 min
# 6000 points = 600 secs = 10 min
# 15 mins = 900 secs = 9000 datapoints
# 20 mins = 1200 secs = 12000 datapoints

# rolling window for mean
r_window = 10
# duration how long an event should be
event_duration_threshold = 120
# time shift
time_shift = 10

# moving average variable
df_day_combined_downsample["pt_0001_load_value_mean"]=df_day_combined_downsample["pt_0001_load_value"].rolling(r_window, min_periods=1).mean()
df_day_combined_downsample["tt_0001_load_value_mean"]=df_day_combined_downsample["tt_0001_load_value"].rolling(r_window, min_periods=1).mean()

df_day_combined_downsample["dp_0001_load_value_mean"]=df_day_combined_downsample["dp_0001_load_value"].rolling(r_window, min_periods=1).mean()
df_day_combined_downsample["dp_0002_load_value_mean"]=df_day_combined_downsample["dp_0002_load_value"].rolling(r_window, min_periods=1).mean()

df_day_combined_downsample["dp_0001_unload_value_mean"]=df_day_combined_downsample["dp_0001_unload_value"].rolling(r_window, min_periods=1).mean()
df_day_combined_downsample["dp_0002_unload_value_mean"]=df_day_combined_downsample["dp_0002_unload_value"].rolling(r_window, min_periods=1).mean()


# %%
# shift variable
df_day_combined_downsample["pt_0001_load_value_mean_shift"]=df_day_combined_downsample["pt_0001_load_value_mean"].shift(periods=time_shift, fill_value=0)
df_day_combined_downsample["tt_0001_load_value_mean_shift"]=df_day_combined_downsample["tt_0001_load_value_mean"].shift(periods=time_shift, fill_value=0)

df_day_combined_downsample["dp_0001_load_value_mean_shift"]=df_day_combined_downsample["dp_0001_load_value_mean"].shift(periods=time_shift, fill_value=0)
df_day_combined_downsample["dp_0002_load_value_mean_shift"]=df_day_combined_downsample["dp_0002_load_value_mean"].shift(periods=time_shift, fill_value=0)

df_day_combined_downsample["dp_0001_unload_value_mean_shift"]=df_day_combined_downsample["dp_0001_unload_value_mean"].shift(periods=time_shift, fill_value=0)
df_day_combined_downsample["dp_0002_unload_value_mean_shift"]=df_day_combined_downsample["dp_0002_unload_value_mean"].shift(periods=time_shift, fill_value=0)

# %%
# mean and shift difference

df_day_combined_downsample["pt_0001_load_value_mean_shift_diff"]=df_day_combined_downsample.apply(lambda x: x["pt_0001_load_value_mean"]-x["pt_0001_load_value_mean_shift"], axis=1)
df_day_combined_downsample["tt_0001_load_value_mean_shift_diff"]=df_day_combined_downsample.apply(lambda x: x["tt_0001_load_value_mean"]-x["tt_0001_load_value_mean_shift"], axis=1)

df_day_combined_downsample["dp_0001_load_value_mean_shift_diff"]=df_day_combined_downsample.apply(lambda x: x["dp_0001_load_value_mean"]-x["dp_0001_load_value_mean_shift"], axis=1)
df_day_combined_downsample["dp_0002_load_value_mean_shift_diff"]=df_day_combined_downsample.apply(lambda x: x["dp_0002_load_value_mean"]-x["dp_0002_load_value_mean_shift"], axis=1)

df_day_combined_downsample["dp_0001_unload_value_mean_shift_diff"]=df_day_combined_downsample.apply(lambda x: x["dp_0001_unload_value_mean"]-x["dp_0001_unload_value_mean_shift"], axis=1)
df_day_combined_downsample["dp_0002_unload_value_mean_shift_diff"]=df_day_combined_downsample.apply(lambda x: x["dp_0002_unload_value_mean"]-x["dp_0002_unload_value_mean_shift"], axis=1)


# %%
plt.figure()
df_day_combined_downsample.plot(x = "time", y = ["pt_0001_load_value", "pt_0001_load_value_mean", "pt_0001_load_value_mean_shift"])
df_day_combined_downsample.plot(x = "time", y = ["tt_0001_load_value", "tt_0001_load_value_mean", "tt_0001_load_value_mean_shift"])
df_day_combined_downsample.plot(x = "time", y = ["dp_0001_load_value", "dp_0001_load_value_mean", "dp_0001_load_value_mean_shift"])
df_day_combined_downsample.plot(x = "time", y = ["dp_0001_unload_value", "dp_0001_unload_value_mean", "dp_0001_unload_value_mean_shift"])
df_day_combined_downsample.plot(x = "time", y = ["dp_0002_load_value", "dp_0002_load_value_mean", "dp_0002_load_value_mean_shift"])
df_day_combined_downsample.plot(x = "time", y = ["dp_0002_unload_value", "dp_0002_unload_value_mean", "dp_0002_unload_value_mean_shift"])

# %%
fig, ax = plt.subplots()
df_day_combined_downsample.hist(ax=ax, column="pt_0001_load_value_mean_shift_diff",bins=20, alpha=0.5)
# df_day_combined.plot(column="dp_0001_load_value_mean_shift_diff", kind = "kde")
ax.set_yscale('log')

fig, ax = plt.subplots()
df_day_combined_downsample.hist(ax=ax, column="tt_0001_load_value_mean_shift_diff",bins=20, alpha=0.5)
ax.set_yscale('log')

fig, ax = plt.subplots()
df_day_combined_downsample.hist(ax=ax, column="dp_0001_load_value_mean_shift_diff",bins=20, alpha=0.5)
ax.set_yscale('log')

fig, ax = plt.subplots()
df_day_combined_downsample.hist(ax=ax, column="dp_0001_unload_value_mean_shift_diff",bins=20, alpha=0.5)
ax.set_yscale('log')


fig, ax = plt.subplots()
df_day_combined_downsample.hist(ax=ax, column="dp_0002_load_value_mean_shift_diff",bins=20, alpha=0.5)
ax.set_yscale('log')


fig, ax = plt.subplots()
df_day_combined_downsample.hist(ax=ax, column="dp_0002_unload_value_mean_shift_diff",bins=20, alpha=0.5)
ax.set_yscale('log')


# %%
fig, ax = plt.subplots(figsize = (6,4))

df_day_combined_downsample.hist(ax=ax, column="dp_0001_load_value_mean_shift_diff",bins=20, alpha=0.5)
ax.set_yscale('log')

# Calculate percentiles
quant_5, quant_25, quant_50, quant_75, quant_95 = df_day_combined_downsample["dp_0001_load_value_mean_shift_diff"].quantile(0.05), df_day_combined_downsample["dp_0001_load_value_mean_shift_diff"].quantile(0.25), df_day_combined_downsample["dp_0001_load_value_mean_shift_diff"].quantile(0.5), df_day_combined_downsample["dp_0001_load_value_mean_shift_diff"].quantile(0.75), df_day_combined_downsample["dp_0001_load_value_mean_shift_diff"].quantile(0.95)

# [quantile, opacity, length]
# quants = [[quant_5, 0.6, 0.16], [quant_25, 0.8, 0.26], [quant_50, 1, 0.36],  [quant_75, 0.8, 0.46], [quant_95, 0.6, 0.56]]
quants = [[quant_5, 0.6, 0.16], [quant_50, 1, 0.36], [quant_95, 0.6, 0.56]]

# Plot the lines with a loop
for i in quants:
    ax.axvline(i[0], alpha = i[1], ymax = i[2], color='r', linestyle = ":")

print("quant_5 = ", quant_5)
print("quant_50 = ", quant_50)
print("quant_95 = ", quant_95)

plt.show()

# %%
# get data for less than 5 quantile
loading_event = df_day_combined_downsample[df_day_combined_downsample["dp_0001_load_value_mean_shift_diff"]<quant_5]

# %%

#based on dp_0001_load_value_mean_shift_diff
past_df = df_day_combined_downsample[df_day_combined_downsample["time"]<="2021-09-01 14:05:17.175664"].tail(60)
future_df = df_day_combined_downsample[df_day_combined_downsample["time"]>"2021-09-01 14:05:17.175664"].head(60)

sig_change_df = pd.concat([past_df,future_df])
sig_change_df[["time"]+variable_list+["dp_0001_load_value_mean","dp_0001_load_value_mean_shift","dp_0001_load_value_mean_shift_diff"]].plot(x='time',subplots=True, sharex=True, figsize=(15,10))

#based on dp_0001_load_value_mean_shift_diff
past_df = df_day_combined_downsample[df_day_combined_downsample["time"]<="2021-09-01 20:23:17.184203"].tail(60)
future_df = df_day_combined_downsample[df_day_combined_downsample["time"]>"2021-09-01 20:23:17.184203"].head(60)

sig_change_df = pd.concat([past_df,future_df])
sig_change_df[["time"]+variable_list+["dp_0001_load_value_mean","dp_0001_load_value_mean_shift","dp_0001_load_value_mean_shift_diff"]].plot(x='time',subplots=True, sharex=True, figsize=(15,10))



# %%

