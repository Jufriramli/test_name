# from filters.butterworth_filter import butter_lowpass_filter
from data.load_vessel_data import load_vessel_data

import numpy as np
import pandas as pd
import glob
import datetime

result_list=[]

# sample to pick out from dataframe
sample_rate = 10 # 10 is 1 secs

# threshold
sensor_event_threshold = 50 
count_threshold = 1200 / sample_rate # set to 120 count (120 secs)
event_duration_threshold = 18000 / sample_rate # set to 30 mins

def getEvent(x):
    
    max_load = max(x.dp_0001_load_value_lpf_mva, x.dp_0002_load_value_lpf_mva)
    max_unload = max(x.dp_0001_unload_value_lpf_mva, x.dp_0002_unload_value_lpf_mva)

    if max_load > sensor_event_threshold and max_load > max_unload and x.pt_0001_load_value < 1:
        return "Loading"
    elif max_unload > sensor_event_threshold and max_unload > max_load and x.pt_0001_load_value >= 1:
        return "Unloading"
    else:
        return ""

def get_event_time_dict(path):
    '''
    path: path to folder of daily readings 

    returns list of dictionary
    [
        {'start': Timestamp('2021-09-15 06:30:57.279058'), 'stop': Timestamp('2021-09-15 16:58:50.277505'), 'event': 'Loading'},
        {'start': Timestamp('2021-09-15 20:25:47.279359'), 'stop': Timestamp('2021-09-15 20:59:46.279316'), 'event': 'Unknown (Loading)'},
        {'start': Timestamp('2021-09-15 22:48:43.277816'), 'stop': None, 'event': 'Unloading'}
    ]
    else returns an empty list if no events

    '''

    # for path in all_filenames:
    df_one_day = load_vessel_data(path, r_window = 6000)
    df_one_day = df_one_day.iloc[::sample_rate]
    df_one_day["time"] = df_one_day.index

    df_one_day["event"]=df_one_day.apply(lambda x: 
        getEvent(x)
        , axis=1
    )

    # group consecutive events
    df_one_day["event_grp"]=(df_one_day.event != df_one_day.event.shift()).cumsum()

    # if signal in file is flat
    if len(df_one_day["event_grp"].unique().tolist())==1:
            # print("No change of state")
            return result_list

    else:
        df_events=pd.DataFrame()
        df_ops=pd.DataFrame()

        ops_list=["Loading", "Unloading"]
        for ops in ops_list:
            df_ops = df_one_day[df_one_day["event"]==ops].groupby("event_grp").agg({"time": [("Start Time",np.min),("End Time",np.max),("Count",np.count_nonzero)]})
            df_ops=df_ops[df_ops["time"]["Count"]>count_threshold]
            if not df_ops.empty:
                df_ops_events_time = pd.DataFrame()
                df_ops_events_time["Start Time"] = df_ops["time"]["Start Time"].apply(lambda x: max(df_one_day["time"].min(), x - datetime.timedelta(minutes=15))) # df_loading["time"]["Start Time"] - datetime.timedelta(minutes=15)
                df_ops_events_time["End Time"] = df_ops["time"]["End Time"].apply(lambda x: min(df_one_day["time"].max(), x + datetime.timedelta(minutes=15))) # df_loading["time"]["End Time"] + datetime.timedelta(minutes=15)
                # df_ops_events_time["Start Time"]=df_ops["time"]["Start Time"].apply(lambda x: max(df_ops["time"]["Start Time"].min(), x - datetime.timedelta(minutes=5))) # df_loading["time"]["Start Time"] - datetime.timedelta(minutes=15)
                # df_ops_events_time["End Time"]=df_ops["time"]["End Time"].apply(lambda x: min(df_ops["time"]["End Time"].max(), x + datetime.timedelta(minutes=5))) # df_loading["time"]["End Time"] + datetime.timedelta(minutes=15)
                df_ops_events_time["Count"]=df_ops["time"]["Count"]
                df_ops_events_time["Event"]=df_ops_events_time["Count"].apply(lambda x: ops if x >= event_duration_threshold else "Unknown ({})".format(ops))

                # join loading and unloading events
                df_events = pd.concat([df_events, df_ops_events_time])

        if not df_events.empty:
            df_events=df_events.sort_values(by='Start Time', ascending=True)
            df_events["duration_min"] = (df_events["End Time"] - df_events["Start Time"]) / pd.Timedelta(minutes=1) # 
            df_events['duration_hr'] = (df_events["End Time"] - df_events["Start Time"]) / pd.Timedelta(hours=1) #

            df_events = df_events.reset_index(drop=True)

            # IMPORTANT! Uncomment to set None if operations spill over to next day
            # # update first and last time stamp if values are high, assume those are events from previous or next day
            # if df_one_day[["dp_0001_load_value",	"dp_0001_unload_value",	"dp_0002_load_value",	"dp_0002_unload_value"]][:150].mean().max(axis=0)>50:
            #     df_events["Start Time"] = df_events["Start Time"].astype(object)
            #     df_events.at[0,'Start Time']=None

            # if df_one_day[["dp_0001_load_value",	"dp_0001_unload_value",	"dp_0002_load_value",	"dp_0002_unload_value"]][-150:].mean().max(axis=0)>50:
            #     df_events["End Time"] = df_events["End Time"].astype(object)
            #     df_events.at[df_events.shape[0]-1,'End Time']=None

            # print(df_events)
            for index, row in df_events.iterrows():
                result_dict={}
                result_dict["start"]=row["Start Time"]
                result_dict["stop"]=row["End Time"]
                result_dict["event"]=row["Event"]
                result_list.append(result_dict)
            print(df_events)
            df_events[["Start Time", "End Time", "Event"]].to_csv("exported_events_timestamp/aa{}.csv".format(pd.to_datetime(df_one_day['time']).dt.date.min().strftime("%Y%m%d")), index=False)
            return result_list

        else:
            # if none of the events longer the threshold time
            # print("No significant events")
            return result_list

#%% single run
# path = "/Users/jufri/Dravam Drive/TS_datavis/KC_AWS/2021/09/30/*.csv"
# result = get_event_time_dict(path)

# multidate_run
date_list = [
    "2021/09/01", "2021/09/02", "2021/09/03", "2021/09/04", "2021/09/05",
    "2021/09/06", "2021/09/07", "2021/09/08", "2021/09/09", "2021/09/10",
    "2021/09/11", "2021/09/12", "2021/09/13", "2021/09/14", "2021/09/15",
    "2021/09/16", "2021/09/17", "2021/09/18", "2021/09/19", "2021/09/20",
    "2021/09/21", "2021/09/22", "2021/09/23", "2021/09/24", "2021/09/25",
    "2021/09/26", "2021/09/27", "2021/09/28", "2021/09/29", "2021/09/30",
    ]

date_list = [
    "2021/10/01", "2021/10/02", "2021/10/03", "2021/10/04", "2021/10/05",
    "2021/10/06", "2021/10/07", "2021/10/08", "2021/10/09", "2021/10/10",
    "2021/10/11", "2021/10/12",
    ]
    
date_list = [
    "2021/09/27"
    ]

for x in date_list:
    path = "/Users/jufri/Dravam Drive/TS_datavis/KC_AWS/"+x+"/*.csv"
    result = get_event_time_dict(path)

