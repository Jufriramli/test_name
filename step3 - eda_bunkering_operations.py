# %%
from filters.butterworth_filter import butter_lowpass_filter

import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

path = "exported_events_raw_data/Unloading/2021-09-07 02_04_12.275993_Unloading.csv"# working
path = "exported_events_raw_data/Unloading/2021-09-07 09_42_13.974229_Unloading.csv"# working
path = "exported_events_raw_data/Unloading/2021-09-07 22_16_42.680488_Unloading.csv"# working
path = "exported_events_raw_data/Unloading/2021-09-07 23_53_17.273194_Unloading.csv"# working
path = "exported_events_raw_data/Unloading/2021-09-08 05_56_29.581773_Unloading.csv"# working

# path = "exported_events_raw_data/Unloading/2021-09-10 06_46_49.184972_Unloading.csv"# working
# path = "exported_events_raw_data/Unloading/2021-09-10 15_12_23.875452_Unloading.csv"# working
# path = "exported_events_raw_data/Unloading/2021-09-10 20_15_08.184682_Unloading.csv"# working

# path = "exported_events_raw_data/Unloading/2021-09-15 23_53_17.277118_Unloading.csv"# working
# path = "exported_events_raw_data/Unloading/2021-09-16 09_27_52.670512_Unloading.csv"# working
# path = "exported_events_raw_data/Unloading/2021-09-16 19_55_10.984498_Unloading.csv"# working
# path = "exported_events_raw_data/Unloading/2021-09-16 23_53_17.278803_Unloading.csv"# working

def dojob(path):
# operation is extracted from last portion of filename (Loading, Unloading, Outlier)
    operation = path.split("_")[-1].replace(".csv","")

    variable_list=[
        "pt_0001_load_value", "tt_0001_load_value", 
        "dp_0001_load_value", "dp_0001_unload_value",
        "dp_0002_load_value", "dp_0002_unload_value"
        ]

    all_filenames = [i for i in glob.glob(path)]
    all_filenames = sorted(all_filenames)
    all_filenames = all_filenames[:]
    # for fname in glob.glob(path):
    #    print(fname)

    # #combine all files in the list
    operation_df = pd.concat([pd.read_csv(f, names=["time"]+variable_list, header=0,  index_col=False) for f in all_filenames ])

    operation_df=operation_df[["time"]+variable_list]

    operation_df['time'] = pd.to_datetime(operation_df['time'], format='%Y-%m-%d %H:%M:%S.%f')

    # sort combined data by time
    operation_df=operation_df.sort_values(by='time', ascending=True)

    # %%

    # Filter requirements.
    order = 2
    fs = 10       # sample rate, Hz
    cutoff = 0.05  # desired cutoff frequency of the filter, Hz

    # operation_df_copy=operation_df.copy()
    # operation_df_copy["dp_0001_unload_value_lpf"] = butter_lowpass_filter(operation_df["dp_0001_unload_value"], cutoff, fs, order)
    # operation_df_copy["dp_0001_unload_value_lpf_mva"]=operation_df_copy["dp_0001_unload_value_lpf"].rolling(100, min_periods=1).mean()

    # operation_df_copy[["time","dp_0001_unload_value","dp_0001_unload_value_lpf","dp_0001_unload_value_mva","dp_0001_unload_value_lpf_mva"]][6000:18000].plot(x="time")

    # operation_df[["time"]+variable_list].plot(x='time',subplots=True, sharex=True, figsize=(15,10))
    # %%

    if operation_df.shape[0]<18000:
        print("Duration is less than 30min.")
        return True # raise SystemExit(0)

    # 1200 = 2 mins
    # 600 = 1 mins
    r_window = 1200
    r_window_lpf = 100 

    # low pass filter load
    operation_df["dp_0001_load_value_lpf"]=butter_lowpass_filter(operation_df["dp_0001_load_value"], cutoff, fs, order)
    operation_df["dp_0002_load_value_lpf"]=butter_lowpass_filter(operation_df["dp_0002_load_value"], cutoff, fs, order)
    operation_df["pt_0001_load_value_lpf"]=butter_lowpass_filter(operation_df["pt_0001_load_value"], cutoff, fs, order)

    # low pass filter unload
    operation_df["dp_0001_unload_value_lpf"]=butter_lowpass_filter(operation_df["dp_0001_unload_value"], cutoff, fs, order)
    operation_df["dp_0002_unload_value_lpf"]=butter_lowpass_filter(operation_df["dp_0002_unload_value"], cutoff, fs, order)
    operation_df["pt_0001_load_value_lpf"]=butter_lowpass_filter(operation_df["pt_0001_load_value"], cutoff, fs, order)

    # moving average
    operation_df["dp_0001_load_value_mva"]=operation_df["dp_0001_load_value"].rolling(r_window, min_periods=1).mean()
    operation_df["dp_0002_load_value_mva"]=operation_df["dp_0002_load_value"].rolling(r_window, min_periods=1).mean()
    operation_df["pt_0001_load_value_mva"]=operation_df["pt_0001_load_value"].rolling(r_window, min_periods=1).mean()
    operation_df["tt_0001_load_value_mva"]=operation_df["tt_0001_load_value"].rolling(r_window, min_periods=1).mean()
    # moving average lpf
    operation_df["dp_0001_load_value_lpf_mva"]=operation_df["dp_0001_load_value_lpf"].rolling(r_window_lpf, min_periods=1).mean()
    operation_df["dp_0002_load_value_lpf_mva"]=operation_df["dp_0002_load_value_lpf"].rolling(r_window_lpf, min_periods=1).mean()
    operation_df["pt_0001_load_value_lpf_mva"]=operation_df["pt_0001_load_value_lpf"].rolling(r_window_lpf, min_periods=1).mean()

    # moving average
    operation_df["dp_0001_unload_value_mva"]=operation_df["dp_0001_unload_value"].rolling(r_window, min_periods=1).mean()
    operation_df["dp_0002_unload_value_mva"]=operation_df["dp_0002_unload_value"].rolling(r_window, min_periods=1).mean()
    # operation_df["pt_0001_load_value_mva"]=operation_df["pt_0001_load_value"].rolling(r_window, min_periods=1).mean()
    # operation_df["tt_0001_load_value_mva"]=operation_df["tt_0001_load_value"].rolling(r_window, min_periods=1).mean()
    # moving average lpf
    operation_df["dp_0001_unload_value_lpf_mva"]=operation_df["dp_0001_unload_value_lpf"].rolling(r_window_lpf, min_periods=1).mean()
    operation_df["dp_0002_unload_value_lpf_mva"]=operation_df["dp_0002_unload_value_lpf"].rolling(r_window_lpf, min_periods=1).mean()
    # operation_df["pt_0001_load_value_lpf_mva"]=operation_df["pt_0001_load_value_lpf"].rolling(r_window_lpf, min_periods=1).mean()

    # normalized against temperature
    operation_df["dp_0001_load_value_norm"]=operation_df.apply(lambda x: x["dp_0001_load_value"]/x["tt_0001_load_value"], axis=1)
    operation_df["dp_0002_load_value_norm"]=operation_df.apply(lambda x: x["dp_0002_load_value"]/x["tt_0001_load_value"], axis=1)
    operation_df["pt_0001_load_value_norm"]=operation_df.apply(lambda x: x["pt_0001_load_value"]/x["tt_0001_load_value"], axis=1)
    # normalized against temperature
    operation_df["dp_0001_unload_value_norm"]=operation_df.apply(lambda x: x["dp_0001_unload_value"]/x["tt_0001_load_value"], axis=1)
    operation_df["dp_0002_unload_value_norm"]=operation_df.apply(lambda x: x["dp_0002_unload_value"]/x["tt_0001_load_value"], axis=1)
    # operation_df["pt_0001_load_value_norm"]=operation_df.apply(lambda x: x["pt_0001_load_value"]/x["tt_0001_load_value"], axis=1)

    # moving average of normalized
    operation_df["dp_0001_load_value_norm_mva"]=operation_df["dp_0001_load_value_norm"].rolling(r_window, min_periods=1).mean()
    operation_df["dp_0002_load_value_norm_mva"]=operation_df["dp_0002_load_value_norm"].rolling(r_window, min_periods=1).mean()
    operation_df["pt_0001_load_value_norm_mva"]=operation_df["pt_0001_load_value"].rolling(r_window, min_periods=1).mean()

    # moving average of normalized
    operation_df["dp_0001_unload_value_norm_mva"]=operation_df["dp_0001_unload_value_norm"].rolling(r_window, min_periods=1).mean()
    operation_df["dp_0002_unload_value_norm_mva"]=operation_df["dp_0002_unload_value_norm"].rolling(r_window, min_periods=1).mean()
    # operation_df["pt_0001_load_value_norm_mva"]=operation_df["pt_0001_load_value"].rolling(r_window, min_periods=1).mean()

    # if operation=="Loading":

    column_dp1_load = ["dp_0001_load_value", "dp_0001_load_value_mva", "dp_0001_load_value_norm", "dp_0001_load_value_norm_mva","dp_0001_load_value_lpf_mva" ]
    column_dp2_load = ["dp_0002_load_value", "dp_0002_load_value_mva", "dp_0002_load_value_norm", "dp_0002_load_value_norm_mva","dp_0002_load_value_lpf_mva" ]


    # elif operation=="Unloading":

    column_dp1_unload = ["dp_0001_unload_value", "dp_0001_unload_value_mva", "dp_0001_unload_value_norm", "dp_0001_unload_value_norm_mva","dp_0001_unload_value_lpf_mva" ]
    column_dp2_unload = ["dp_0002_unload_value", "dp_0002_unload_value_mva", "dp_0002_unload_value_norm", "dp_0002_unload_value_norm_mva","dp_0002_unload_value_lpf_mva" ]

    column_dp1 = column_dp1_load + column_dp1_unload
    column_dp2 = column_dp2_load + column_dp2_unload

    column_pt1 = ["pt_0001_load_value", "pt_0001_load_value_mva", "pt_0001_load_value_norm", "pt_0001_load_value_norm_mva","pt_0001_load_value_lpf_mva" ]
    column_tt1 = ["tt_0001_load_value", "tt_0001_load_value_mva"]

    column_list = [column_dp1,column_dp2,column_pt1,column_tt1]

    operation_df.plot(x='time',subplots=True, sharex=True, figsize=(15,10))

    # %%

    # copy dataframe to scale
    df_min_max_scaled = operation_df.copy()

    # apply normalization techniques by Column
    df_min_max_scaled[column_dp1] = (df_min_max_scaled[column_dp1] - df_min_max_scaled[column_dp1].min()) / (df_min_max_scaled[column_dp1].max() - df_min_max_scaled[column_dp1].min())    
    df_min_max_scaled[column_dp2] = (df_min_max_scaled[column_dp2] - df_min_max_scaled[column_dp2].min()) / (df_min_max_scaled[column_dp2].max() - df_min_max_scaled[column_dp2].min())    
    df_min_max_scaled[column_pt1] = (df_min_max_scaled[column_pt1] - df_min_max_scaled[column_pt1].min()) / (df_min_max_scaled[column_pt1].max() - df_min_max_scaled[column_pt1].min())    
    df_min_max_scaled[column_tt1] = (df_min_max_scaled[column_tt1] - df_min_max_scaled[column_tt1].min()) / (df_min_max_scaled[column_tt1].max() - df_min_max_scaled[column_tt1].min())    
    
    # view normalized data
    df_min_max_scaled[["time"]+column_dp1].plot(x="time",figsize=(15,10))
    df_min_max_scaled[["time"]+column_dp2].plot(x="time",figsize=(15,10))
    df_min_max_scaled[["time"]+column_pt1].plot(x="time",figsize=(15,10))
    df_min_max_scaled[["time"]+column_tt1].plot(x="time",figsize=(15,10))

    # %%

    # 30 min = 10*60*30 = 18000

    timeframe = 18000

    start_timeframe =  timeframe # int(df_min_max_scaled.shape[0]*.4)
    end_timeframe = timeframe # int(df_min_max_scaled.shape[0]*.3)

    for col in column_list:
        df_min_max_scaled[["time"]+col].plot(x="time",figsize=(15,10))
        # first 30 min
        df_min_max_scaled[["time"]+col][:start_timeframe].plot(x="time",figsize=(15,10))
        # middle 
        df_min_max_scaled[["time"]+col][start_timeframe:df_min_max_scaled.shape[0]-end_timeframe].plot(x="time",figsize=(15,10))
        #last 30 min
        df_min_max_scaled[["time"]+col][-end_timeframe:].plot(x="time",figsize=(15,10))

    # %%

    rms_interval = 600 # 1 min 1800 # 3 mins

    first_frame = [f for f in range(0, start_timeframe, rms_interval)]
    last_frame = [f for f in range(end_timeframe, 1, -rms_interval)]

    # first_frame = [0, 3000, 6000, 9000, 12000, 15000]
    # last_frame = [15000, 12000, 9000, 6000, 3000, 1]
    start_index =  ['0-5', '5-10', '10-15', '15-20', '20-25', '25-30']
    end_index =  ['-(30-25)', '-(25-20)', '-(20-15)', '-(15-10)', '-(10-5)', '-(5-0)']

    rms_list = []
    avg_list = []

    ops_start_dict={}
    ops_end_dict={}
    ops_middle_dict={}

    for col in column_dp1+column_dp2+column_pt1+column_tt1:
        for x in first_frame:
            # print (x,((df_min_max_scaled[col][x:x+3000]) ** 2).mean() ** .5)
            rms_list.append((((df_min_max_scaled[col][x:x+rms_interval]) ** 2).mean() ** .5))
            # temp get average
            rms_list.append((df_min_max_scaled[col][x:x+rms_interval].mean()))
        ops_start_dict[col] = rms_list
        rms_list = []

    ops_start_df = pd.DataFrame.from_dict(ops_start_dict)

    for col in column_dp1+column_dp2+column_pt1+column_tt1:
        for x in last_frame:
            # print (((df_min_max_scaled[col][-3000-x:-x]) ** 2).mean() ** .5)
            rms_list.append((((df_min_max_scaled[col][-rms_interval-x:-x]) ** 2).mean() ** .5))
            # temp get average
            rms_list.append((df_min_max_scaled[col][-rms_interval-x:-x].mean()))
        ops_end_dict[col] = rms_list
        rms_list = []

    ops_end_df = pd.DataFrame.from_dict(ops_end_dict)

    for col in column_dp1+column_dp2+column_pt1+column_tt1:
        for x in range(start_timeframe,df_min_max_scaled.shape[0]-end_timeframe,rms_interval):
            # print (x,((df_min_max_scaled[col][x:x+3000]) ** 2).mean() ** .5)
            rms_list.append((((df_min_max_scaled[col][x:x+rms_interval]) ** 2).mean() ** .5))
            # temp get average
            rms_list.append((df_min_max_scaled[col][x:x+rms_interval].mean()))
        ops_middle_dict[col] = rms_list
        rms_list = []

    ops_middle_df = pd.DataFrame.from_dict(ops_middle_dict)

    # %%
    # RMS Plot
    # for col in column_list:
    #     end_to_end_col_df = pd.concat([ops_start_df[col],ops_middle_df[col],ops_end_df[col]], ignore_index=True)
    #     end_to_end_col_df.plot(figsize=(15,6)).get_figure().savefig("eda_export/"+path.split("/")[2][:-4]+"_"+col[0][0:7]+"_0.png")
    #     ops_start_df[col].plot().get_figure().savefig("eda_export/"+path.split("/")[2][:-4]+"_"+col[0][0:7]+"_1.png")
    #     ops_middle_df[col].plot().get_figure().savefig("eda_export/"+path.split("/")[2][:-4]+"_"+col[0][0:7]+"_2.png")
    #     ops_end_df[col].plot().get_figure().savefig("eda_export/"+path.split("/")[2][:-4]+"_"+col[0][0:7]+"_3.png")

    # %%
    import plotly.graph_objects as go
    from plotly.subplots import make_subplots

    end_to_end_df = pd.concat([ops_start_df,ops_middle_df,ops_end_df], ignore_index=True)
    end_to_end_df[[col[1] for col in column_list]].plot(figsize=(15,6)).get_figure().savefig("eda_export/"+path.split("/")[2][:-4]+"_end_to_end.png")

    # fig.show()

    # %%

    # set up plotly figure

    # for col_list in [column_dp1,column_dp2,column_pt1,column_tt1]:
    #     fig = go.Figure()
    #     # print(col_list)
    #     fig.update_layout(
    #         title_text="Bunkering Operation")
    #     for col in col_list:
    #         # print(col)
    #         # add line / trace 1 to figure
    #         fig.add_trace(go.Scatter(
    #             name=col,
    #             x=end_to_end_df.index,
    #             y=end_to_end_df[col],
    #             hovertext=end_to_end_df[col],
    #             # hoverinfo="text",
    #             # marker=dict(
    #             #     color="blue"
    #             # ),
    #             showlegend=True
    #         ))
    #     # fig.show()
    #     fig.write_html("eda_export/"+path.split("/")[2][:-4]+"_"+col[0:7]+".html")


    # %%
    # AD HOC gradient specific
    # https://het.as.utexas.edu/HET/Software/Numpy/reference/generated/numpy.gradient.html

    # RMS
    # end_to_end_df[["dp_0001_unload_value_lpf_mva","dp_0002_unload_value_lpf_mva","tt_0001_load_value_mva"]].plot(subplots=True,figsize=(15,10))

    # if operation=="Loading":
        # gradient_col=["dp_0001_load_value_lpf_mva","dp_0002_load_value_lpf_mva","dp_0001_unload_value_lpf_mva","dp_0002_unload_value_lpf_mva","tt_0001_load_value_mva"]
        # gradient_col=["dp_0001_load_value_lpf_mva","dp_0002_load_value_lpf_mva","dp_0001_unload_value_lpf_mva","dp_0002_unload_value_lpf_mva","tt_0001_load_value_mva","pt_0001_load_value_mva"]
    # elif operation=="Unloading":
        # gradient_col=["dp_0001_load_value_lpf_mva","dp_0002_load_value_lpf_mva","dp_0001_unload_value_lpf_mva","dp_0002_unload_value_lpf_mva","tt_0001_load_value_mva"]
    gradient_col=["dp_0001_load_value_lpf_mva","dp_0002_load_value_lpf_mva","dp_0001_unload_value_lpf_mva","dp_0002_unload_value_lpf_mva","tt_0001_load_value_mva","pt_0001_load_value_mva"]

    df_min_max_scaled[["time"]+gradient_col].plot(x="time",subplots=True,figsize=(15,10))

    gradient_df = pd.DataFrame()
    gradient_window_df = pd.DataFrame()

    for x in gradient_col:
        gradient_df[x] = np.gradient(np.array(df_min_max_scaled[x], dtype=np.float),1)
        gradient_window_df[x] = np.gradient(np.array(end_to_end_df[x], dtype=np.float),1)

    gradient_df.index=df_min_max_scaled.time
    gradient_df[:].plot(subplots=True,figsize=(15,10),title="Gradients for "+path.split("/")[2][:-4])
    plt.savefig('eda_export/mfa_gradient_'+path.split("/")[2][:-4]+'.png', facecolor='white') # saves the current figure
    # gradient_df[:36000].plot(subplots=True,figsize=(15,10))
    # plt.savefig('eda_export/gradient_'+path.split("/")[2][:-4]+'_first2hr.png', facecolor='white') # saves the current figure

    # gradient_window_df[:].plot(subplots=True,figsize=(15,10))
    # plt.savefig('eda_export/window_gradient_'+path.split("/")[2][:-4]+'.png', facecolor='white') # saves the current figure

    operation_df[["time"]+gradient_col][:].plot(x="time",subplots=True,figsize=(15,10), title="Processed readings for "+path.split("/")[2][:-4])
    plt.savefig('eda_export/mfa_rawlpf_'+path.split("/")[2][:-4]+'.png', facecolor='white') # saves the current figure
    # operation_df[["time"]+gradient_col][:36000].plot(x="time",subplots=True,figsize=(15,10))
    # plt.savefig('eda_export/rawlpf_'+path.split("/")[2][:-4]+'_first2hr.png', facecolor='white') # saves the current figure

    # df_min_max_scaled["dp_0001_unload_value_lpf_mva"].diff().plot(subplots=True,figsize=(15,10))

    # # %%
    # # adhoc to print standard deviation
    # for std_col in gradient_df.columns:
    #     if std_col=="dp_0001_unload_value_lpf_mva" or std_col=="dp_0002_unload_value_lpf_mva":
    #         with open("gradient_std.txt", "a") as f:
    #             print(path.split("/")[2][:-4], file=f)
    #             print("DP1 Unload" if std_col=="dp_0001_unload_value_lpf_mva" else "DP2 Unload", file=f)
    #             print("RMS_B: {0:.2f}".format(gradient_df[std_col][:9000].std()*10000), file=f)
    #             print("RMS_M: {0:.2f}".format(gradient_df[std_col][9000:-9000].std()*10000), file=f)
    #             print("RMS_E: {0:.2f}".format(gradient_df[std_col][-9000:].std()*10000), file=f)
    #             print("", file=f)

    # %% 

    # # plotly graph for processed signals
    # if operation=="Loading":
    #     plotly_processed = ["dp_0001_load_value_lpf_mva","dp_0002_load_value_lpf_mva","dp_0001_unload_value_lpf_mva","dp_0002_unload_value_lpf_mva","tt_0001_load_value_mva"]
    # elif operation=="Unloading":
    #     plotly_processed = ["dp_0001_load_value_lpf_mva","dp_0002_load_value_lpf_mva","dp_0001_unload_value_lpf_mva","dp_0002_unload_value_lpf_mva","tt_0001_load_value_mva"]

    # fig = make_subplots(rows=len(plotly_processed), cols=1, vertical_spacing=0.065, shared_xaxes=True)
    # for idx, plotly_processed_var in enumerate(plotly_processed):
    #     fig.add_trace(
    #         go.Scatter(x=list(df_min_max_scaled.time), y=list(df_min_max_scaled[plotly_processed_var]), name=plotly_processed_var), idx+1, 1)

    # fig.update_layout(
    #     height=800,
    #     title_text="Bunkering Operation")

    # # fig.show()
    # fig.write_html("exported_html/"+path.split("/")[2][:-4]+".html")

    # %%
    plt.close('all')

# %%

# ops_filenames = [i for i in glob.glob("exported_events_raw_data/Loading/2021-09-30 00_08_17.279962_Loading.csv")]
# ops_filenames = [i for i in glob.glob("exported_events_raw_data/temp/2021-09-30*.csv")]
ops_filenames = [i for i in glob.glob("/Users/jufri/SynologyDrive/files_exports/exported_events_raw_data/2021-10*.csv")]
ops_filenames = sorted(ops_filenames)

for x in ops_filenames:
    print(x)
    dojob(x)