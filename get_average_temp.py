from data.load_vessel_data import load_vessel_data

import numpy as np
import glob
import pandas as pd

variable_list=[
    "pt_0001_load_value", "tt_0001_load_value", 
    "dp_0001_load_value", "dp_0001_unload_value",
    "dp_0002_load_value", "dp_0002_unload_value"
    ]

ops_filenames = [i for i in glob.glob("exported_events_raw_data/Loading/*.csv")]
ops_filenames = sorted(ops_filenames)

temp_list =[]

for f in ops_filenames:
    ops_df = pd.read_csv(f, names=["time"]+variable_list, header=0,  index_col=False) 
    temp_list.append(ops_df["tt_0001_load_value"].mean())

print(temp_list)
print(sum(temp_list) / len(temp_list))
