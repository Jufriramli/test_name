import glob
import pandas as pd
import numpy as np

path = "exported_events_raw_data/Loading/2021-09-19 02_21_53.380692_Loading.csv" # less than 1hr
path = "exported_events_raw_data/Loading/2021-09-12 00_08_17.279158_Loading.csv" # less than 1hr
# path = "exported_events_raw_data/Unloading/2021-09-22 07_07_54.278606_Unloading.csv" # working
# path = "exported_events_raw_data/Loading/2021-09-01 13_32_10.269771_Loading.csv"# working

# operation is extracted from last portion of filename (Loading, Unloading, Outlier)
operation = path.split("_")[-1].replace(".csv","")

header_list = [
    "time", "date_year", "date_month", "date_day", 
    "time_hr", "time_min", "time_sec", "time_Msec", 
    "pt_0001_load_value", "tt_0001_load_value", # pressure, temp
    "dp_0001_load_value", "dp_0001_unload_value", # diff pressure low 0-2000, high 0-5000
    "dp_0002_load_value", "dp_0002_unload_value",
    ]

variable_list=[
    "pt_0001_load_value", "tt_0001_load_value", 
    "dp_0001_load_value", "dp_0001_unload_value",
    "dp_0002_load_value", "dp_0002_unload_value"
    ]

all_filenames = [i for i in glob.glob(path)]
all_filenames = sorted(all_filenames)
all_filenames = all_filenames[:]
# for fname in glob.glob(path):
#    print(fname)

# #combine all files in the list
operation_df = pd.concat([pd.read_csv(f, names=["time"]+variable_list, header=0,  index_col=False) for f in all_filenames ])

operation_df=operation_df[["time"]+variable_list]

operation_df['time'] = pd.to_datetime(operation_df['time'], format='%Y-%m-%d %H:%M:%S.%f')

# sort combined data by time
operation_df=operation_df.sort_values(by='time', ascending=True)

# operation_df[["time"]+variable_list].plot(x='time',subplots=True, sharex=True, figsize=(15,10))
# %%

if operation_df.shape[0]<18000:
    print("Duration is less than 1hr.")
    raise SystemExit(0)

# 1200 = 2 mins
# 600 = 1 mins
r_window=1200

if operation=="Loading":

    # moving average
    operation_df["dp_0001_load_value_mva"]=operation_df["dp_0001_load_value"].rolling(r_window, min_periods=1).mean()
    operation_df["dp_0002_load_value_mva"]=operation_df["dp_0002_load_value"].rolling(r_window, min_periods=1).mean()
    operation_df["pt_0001_load_value_mva"]=operation_df["pt_0001_load_value"].rolling(r_window, min_periods=1).mean()

    # normalized against temperature
    operation_df["dp_0001_load_value_norm"]=operation_df.apply(lambda x: x["dp_0001_load_value"]/x["tt_0001_load_value"], axis=1)
    operation_df["dp_0002_load_value_norm"]=operation_df.apply(lambda x: x["dp_0002_load_value"]/x["tt_0001_load_value"], axis=1)
    operation_df["pt_0001_load_value_norm"]=operation_df.apply(lambda x: x["pt_0001_load_value"]/x["tt_0001_load_value"], axis=1)
        
    # moving average of normalized
    operation_df["dp_0001_load_value_norm_mva"]=operation_df["dp_0001_load_value_norm"].rolling(r_window, min_periods=1).mean()
    operation_df["dp_0002_load_value_norm_mva"]=operation_df["dp_0002_load_value_norm"].rolling(r_window, min_periods=1).mean()
    operation_df["pt_0001_load_value_norm_mva"]=operation_df["pt_0001_load_value"].rolling(r_window, min_periods=1).mean()

    column_dp1 = ["dp_0001_load_value", "dp_0001_load_value_mva", "dp_0001_load_value_norm", "dp_0001_load_value_norm_mva" ]
    column_dp2 = ["dp_0002_load_value", "dp_0002_load_value_mva", "dp_0002_load_value_norm", "dp_0002_load_value_norm_mva" ]
    column_pt1 = ["pt_0001_load_value", "pt_0001_load_value_mva", "pt_0001_load_value_norm", "pt_0001_load_value_norm_mva" ]

elif operation=="Unloading":

    # moving average
    operation_df["dp_0001_unload_value_mva"]=operation_df["dp_0001_unload_value"].rolling(r_window, min_periods=1).mean()
    operation_df["dp_0002_unload_value_mva"]=operation_df["dp_0002_unload_value"].rolling(r_window, min_periods=1).mean()
    operation_df["pt_0001_load_value_mva"]=operation_df["pt_0001_load_value"].rolling(r_window, min_periods=1).mean()

    # normalized against temperature
    operation_df["dp_0001_unload_value_norm"]=operation_df.apply(lambda x: x["dp_0001_unload_value"]/x["tt_0001_load_value"], axis=1)
    operation_df["dp_0002_unload_value_norm"]=operation_df.apply(lambda x: x["dp_0002_unload_value"]/x["tt_0001_load_value"], axis=1)
    operation_df["pt_0001_load_value_norm"]=operation_df.apply(lambda x: x["pt_0001_load_value"]/x["tt_0001_load_value"], axis=1)
        
    # moving average of normalized
    operation_df["dp_0001_unload_value_norm_mva"]=operation_df["dp_0001_unload_value_norm"].rolling(r_window, min_periods=1).mean()
    operation_df["dp_0002_unload_value_norm_mva"]=operation_df["dp_0002_unload_value_norm"].rolling(r_window, min_periods=1).mean()
    operation_df["pt_0001_load_value_norm_mva"]=operation_df["pt_0001_load_value"].rolling(r_window, min_periods=1).mean()
    
    column_dp1 = ["dp_0001_unload_value", "dp_0001_unload_value_mva", "dp_0001_unload_value_norm", "dp_0001_unload_value_norm_mva" ]
    column_dp2 = ["dp_0002_unload_value", "dp_0002_unload_value_mva", "dp_0002_unload_value_norm", "dp_0002_unload_value_norm_mva" ]
    column_pt1 = ["pt_0001_load_value", "pt_0001_load_value_mva", "pt_0001_load_value_norm", "pt_0001_load_value_norm_mva" ]

operation_df.plot(x='time',subplots=True, sharex=True, figsize=(15,10))

# %%

# copy dataframe to scale
df_min_max_scaled = operation_df.copy()

# apply normalization techniques by Column
df_min_max_scaled[column_dp1] = (df_min_max_scaled[column_dp1] - df_min_max_scaled[column_dp1].min()) / (df_min_max_scaled[column_dp1].max() - df_min_max_scaled[column_dp1].min())    
df_min_max_scaled[column_dp2] = (df_min_max_scaled[column_dp2] - df_min_max_scaled[column_dp2].min()) / (df_min_max_scaled[column_dp2].max() - df_min_max_scaled[column_dp2].min())    
df_min_max_scaled[column_pt1] = (df_min_max_scaled[column_pt1] - df_min_max_scaled[column_pt1].min()) / (df_min_max_scaled[column_pt1].max() - df_min_max_scaled[column_pt1].min())    
  
# view normalized data
df_min_max_scaled[["time"]+column_dp1].plot(x="time",figsize=(15,10))
df_min_max_scaled[["time"]+column_dp2].plot(x="time",figsize=(15,10))
df_min_max_scaled[["time"]+column_pt1].plot(x="time",figsize=(15,10))

# %%

# 30 min = 10*60*30 = 18000

timeframe = 18000

# column_dp1
# first 30 min
df_min_max_scaled[["time"]+column_dp1][:timeframe+1].plot(x="time",figsize=(15,10))
#last 30 min
df_min_max_scaled[["time"]+column_dp1][-timeframe:].plot(x="time",figsize=(15,10))
# middle 
df_min_max_scaled[["time"]+column_dp1][timeframe+1:df_min_max_scaled.shape[0]-timeframe].plot(x="time",figsize=(15,10))

# column_dp2
# first 30 min
df_min_max_scaled[["time"]+column_dp2][:timeframe+1].plot(x="time",figsize=(15,10))
#last 30 min
df_min_max_scaled[["time"]+column_dp2][-timeframe:].plot(x="time",figsize=(15,10))
# middle 
df_min_max_scaled[["time"]+column_dp2][timeframe+1:df_min_max_scaled.shape[0]-timeframe].plot(x="time",figsize=(15,10))

# column_pt1
# first 30 min
df_min_max_scaled[["time"]+column_pt1][:timeframe+1].plot(x="time",figsize=(15,10))
#last 30 min
df_min_max_scaled[["time"]+column_pt1][-timeframe:].plot(x="time",figsize=(15,10))
# middle 
df_min_max_scaled[["time"]+column_pt1][timeframe+1:df_min_max_scaled.shape[0]-timeframe].plot(x="time",figsize=(15,10))

# %%
first_frame = [0, 3000, 6000, 9000, 12000, 15000]
last_frame = [15000, 12000, 9000, 6000, 3000, 1]
start_index =  ['0-5', '5-10', '10-15', '15-20', '20-25', '25-30']
end_index =  ['-(30-25)', '-(25-20)', '-(20-15)', '-(15-10)', '-(10-5)', '-(5-0)']

rms_list = []
ops_start_dict={}
ops_end_dict={}
ops_middle_dict={}

for col in column_dp1+column_dp2+column_pt1:
    for x in first_frame:
        # print (x,((df_min_max_scaled[col][x:x+3000]) ** 2).mean() ** .5)
        rms_list.append((((df_min_max_scaled[col][x:x+3000]) ** 2).mean() ** .5))
    ops_start_dict[col] = rms_list
    rms_list = []

ops_start_df = pd.DataFrame.from_dict(ops_start_dict)
ops_start_df.index = start_index

for col in column_dp1+column_dp2+column_pt1:
    for x in last_frame:
        # print (((df_min_max_scaled[col][-3000-x:-x]) ** 2).mean() ** .5)
        rms_list.append((((df_min_max_scaled[col][-3000-x:-x]) ** 2).mean() ** .5))
    ops_end_dict[col] = rms_list
    rms_list = []

ops_end_df = pd.DataFrame.from_dict(ops_end_dict)
ops_end_df.index = end_index    

for col in column_dp1+column_dp2+column_pt1:
    for x in range(3000,df_min_max_scaled.shape[0]-15000,3000):
        # print (x,((df_min_max_scaled[col][x:x+3000]) ** 2).mean() ** .5)
        rms_list.append((((df_min_max_scaled[col][x:x+3000]) ** 2).mean() ** .5))
    ops_middle_dict[col] = rms_list
    rms_list = []

ops_middle_df = pd.DataFrame.from_dict(ops_middle_dict)

# %%
# RMS Plot
ops_start_df[column_dp1].plot()
ops_start_df[column_dp2].plot()
ops_start_df[column_pt1].plot()

ops_end_df[column_dp1].plot()
ops_end_df[column_dp2].plot()
ops_end_df[column_pt1].plot()

ops_middle_df[column_dp1].plot()
ops_middle_df[column_dp2].plot()
ops_middle_df[column_pt1].plot()

# %%
