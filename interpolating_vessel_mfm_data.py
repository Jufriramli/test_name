from numpy.core.fromnumeric import size
from data.load_vessel_data import load_vessel_data
from data.load_mfm_data import load_mfm_data
import glob

import pandas as pd

df_vessel_and_mfm=pd.DataFrame()

mfm_path=r"Mass Flow_Standard Density/*.csv"
df_mfm_data=load_mfm_data(mfm_path)

all_foldername = [i for i in glob.glob(r"KC_AWS/2021/09/*/")]
all_foldername = sorted(all_foldername)
all_foldername = all_foldername[15:]

for foldername in all_foldername:

    vessel_path=foldername+"*.csv"
    # print(vessel_path)
    df_vessel_data=load_vessel_data(vessel_path)

    df_vessel_data.index = df_vessel_data.index.floor('S')
    # df_vessel_data = df_vessel_data[::20]

    df_vessel_and_mfm_temp = pd.merge(df_mfm_data, df_vessel_data, left_index=True, right_index=True)[::10]

    df_vessel_and_mfm = pd.concat([df_vessel_and_mfm, df_vessel_and_mfm_temp])

df_vessel_and_mfm.columns
df_vessel_and_mfm[["Mass Flow_lpf", "Mass Flow_lpf_mva"]].plot(figsize=(15,10))

df_vessel_and_mfm[(df_vessel_and_mfm.index>pd.to_datetime('2021-09-24 05:00:00'))&(df_vessel_and_mfm.index<pd.to_datetime('2021-09-24 06:00:00'))][["Mass Flow", "Mass Flow_lpf"]].plot(figsize=(15,10))
df_vessel_and_mfm[(df_vessel_and_mfm.index>pd.to_datetime('2021-09-24 05:40:00'))&(df_vessel_and_mfm.index<pd.to_datetime('2021-09-24 05:50:00'))][["dp_0001_unload_value", "dp_0001_unload_value_lpf", "dp_0001_unload_value_lpf_mva"]].plot(figsize=(15,10))
df_vessel_and_mfm[(df_vessel_and_mfm.index>pd.to_datetime('2021-09-24 05:40:00'))&(df_vessel_and_mfm.index<pd.to_datetime('2021-09-24 05:50:00'))][["dp_0001_unload_value_lpf_mva_norm", "dp_0002_unload_value_lpf_mva_norm"]].plot(figsize=(15,10))