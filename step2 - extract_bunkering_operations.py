# this script combines the events in exported_events_timestamp and output this combined list as csv
# it also plot daily events
# also export the raw data of bunkering events and plot individual bunkering event

from pandas.core.reshape.concat import concat
from filters.butterworth_filter import butter_lowpass_filter
from data.load_vessel_data import load_vessel_data


import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def group_events(events_list):
    start = 0
    group_sequence = []
    # df_events['Event'].to_list()
    previous=None
    for e in events_list.to_list():
        if previous != 'Loading' and e == 'Loading':
            if start==0:
                start=1
            else:
                start=start+1
        group_sequence.append(start)
        previous=e
    #pd.DataFrame({"start": df_events['Start Time'].tolist() ,"events": df_events['Event'].tolist(), "group": group_sequence}, index=df_events['Event'].index).to_csv("checkme.csv")
    return group_sequence

def unloading_number(events_list):
    start = 0
    previous=None
    unloading_list=[]
    for e in events_list.to_list():
        if (previous == 'Loading' or previous == None) and e != 'Loading':
            start = 1
        elif e == 'Loading':
            start = 0
        else:
            start=start+1
        unloading_list.append(start)
        previous=e
    return unloading_list

def ops_intermittency(events_list, events_start, events_stop):
    events_start = pd.to_datetime(events_start)
    events_stop = pd.to_datetime(events_stop)
    previous = None
    intermittency = 0.0
    previous_stop_time = None
    intermittency_list = []
    # for e, start, stop in enumerate(events_list.to_list(), events_start.to_list(), events_stop.to_list()):
    for idx in range(len(events_list)):
        if previous == "Loading" and events_list.iloc[idx] == "Loading" and previous != None:
            intermittency = max(0.01, (pd.to_datetime(events_start.iloc[idx])-previous_stop_time) / pd.Timedelta(hours=1)) # pd.Timedelta(events_start.iloc[idx] - previous_stop_time).seconds / 3600.0
        elif  previous != "Loading" and events_list.iloc[idx] != "Loading" and previous != None:
            intermittency = max(0.01, (pd.to_datetime(events_start.iloc[idx])-previous_stop_time) / pd.Timedelta(hours=1)) # pd.Timedelta(events_start.iloc[idx] - previous_stop_time).seconds / 3600.0
        else:
            intermittency = 0.0
        previous_stop_time = events_stop.iloc[idx]
        previous=events_list.iloc[idx]
        intermittency_list.append(max(0.0, intermittency))
    return intermittency_list        

def get_df_events():
    all_filenames = [i for i in glob.glob("exported_events_timestamp/aa20210929.csv")]
    all_filenames = sorted(all_filenames)
    all_filenames = all_filenames[:]
    # for fname in glob.glob(path):
    #    print(fname)

    event_list = ["Start Time", "End Time", "Event"]

    # #combine all files in the list
    df_events = pd.concat([pd.read_csv(f, header=0, names=event_list) for f in all_filenames ])
    # df_events.to_csv("checkmetoo.csv")

    df_events['End Time ts'] = pd.to_datetime(df_events['End Time'], format='%Y-%m-%d %H:%M:%S.%f')
    df_events['Start Time ts'] = pd.to_datetime(df_events['Start Time'], format='%Y-%m-%d %H:%M:%S.%f')

    df_events['duration']=(df_events['End Time ts']-df_events['Start Time ts'])
    df_events['duration_tot_hour_diff'] = (df_events['End Time ts']-df_events['Start Time ts']) / pd.Timedelta(hours=1)
    df_events['duration_tot_minutes_diff'] = (df_events['End Time ts']-df_events['Start Time ts']) / pd.Timedelta(minutes=1)

    df_events = df_events[df_events["Event"]!="Outlier"]
    df_events[["Start Time",	"End Time",	"Event",	"duration",	"duration_tot_hour_diff",	"duration_tot_minutes_diff"]].to_csv("combined_events.csv", index=False)

    # format for combined export
    df_events_format = df_events.copy()
    df_events_format["Cycle"]=group_events(df_events_format['Event'])
    df_events_format["Unloading Number"]=unloading_number(df_events_format['Event'])

    df_events_format["St_date"] = pd.to_datetime(df_events['Start Time']).dt.strftime('%Y%m%d')
    df_events_format["St_time"] = pd.to_datetime(df_events['Start Time']).dt.strftime('%H%M')
    df_events_format["End_time"] = pd.to_datetime(df_events['End Time']).dt.strftime('%H%M')
    
    df_events_format["Duration (h)"] = (pd.to_datetime(df_events['End Time'])-pd.to_datetime(df_events['Start Time'])) / pd.Timedelta(hours=1)

    df_events_format["Ops_Id"] = df_events_format.apply(lambda x: "L"+str(x["Cycle"])+("U"+str(x["Unloading Number"]) if x["Event"]!="Loading" else "" )+"-"+x["St_date"]+"-"+x["St_time"]+"-"+x["End_time"], axis=1)
    df_events_format["Ops_Intermittency"]=ops_intermittency(df_events_format['Event'], df_events['Start Time'], df_events['End Time'])
    
    df_events_format[[
        "Start Time",	
        "End Time",	
        "Event",
        "Cycle",
        "Unloading Number",
        "St_date",
        "St_time",
        "End_time",
        "Ops_Id",
        "Ops_Intermittency",
    ]].to_csv("combined_events_format.csv", index=False)
    return df_events

def extract_bunkering_operations(vessel_data_path, df_events):

    df_day_combined = load_vessel_data(vessel_data_path)

    # Export daily plot
    df_day_combined[["pt_0001_load_value", "tt_0001_load_value", "dp_0001_load_value_lpf", "dp_0002_load_value_lpf", "dp_0001_unload_value_lpf", "dp_0002_unload_value_lpf"]].plot(subplots=True, sharex=True, figsize=(15,10))
    plt.savefig('export_bunker_plot/'+("-".join(vessel_data_path.split("/")[-4:-1]))+'.png') # saves the current figure

    for index, row in df_events.iterrows():
        # print(index, row)
        if ((pd.to_datetime(row["Start Time"])<df_day_combined.index.to_series().max()) & (pd.to_datetime(row["Start Time"])>=df_day_combined.index.to_series().min())): # not df_day_combined[(df_day_combined["time"]>=row["Start Time"])].empty: # not export_events_df.empty:
            export_events_df = df_day_combined[(df_day_combined.index.to_series()>=row["Start Time"]) & (df_day_combined.index.to_series()<=row["End Time"])]
            export_events_df.to_csv("/Users/jufri/SynologyDrive/files_exports/exported_events_raw_data/"+row["Start Time"].replace(":","_")+"_"+row["Event"]+".csv")
            # (df_day_combined['date'] > '2000-6-1') & (df_day_combined['date'] <= '2000-6-10')
            export_events_lpf_df=export_events_df.copy()

            export_events_lpf_df[["pt_0001_load_value", "tt_0001_load_value", "dp_0001_load_value_lpf", "dp_0002_load_value_lpf", "dp_0001_unload_value_lpf", "dp_0002_unload_value_lpf"]].plot(subplots=True, sharex=True, figsize=(15,10))
            plt.savefig('export_bunker_plot/'+row["Start Time"].replace(":","_")+'_'+row["Event"]+'.png') # saves the current figure
            # export_events_lpf_df[["dp_0001_unload_value", "dp_0001_unload_value_lpf"]].plot(figsize=(15,10))  
            # print("Entered {}".format(row["Start Time"]))

# %%
# # single use
# date="2021/09/06"
# path = "/Users/jufri/Dravam Drive/TS_datavis/KC_AWS/"+date+"/*.csv"
# extract_bunkering_operations(path)

# multidate_run
date_list = [
    "2021/09/01", "2021/09/02", "2021/09/03", "2021/09/04", "2021/09/05",
    "2021/09/06", "2021/09/07", "2021/09/08", "2021/09/09", "2021/09/10",
    "2021/09/11", "2021/09/12", "2021/09/13", "2021/09/14", "2021/09/15",
    "2021/09/16", "2021/09/17", "2021/09/18", "2021/09/19", "2021/09/20",
    "2021/09/21", "2021/09/22", "2021/09/23", "2021/09/24", "2021/09/25",
    "2021/09/26", "2021/09/27", "2021/09/28", "2021/09/29", "2021/09/30",
    ]

date_list = [
    "2021/10/01", "2021/10/02", "2021/10/03", "2021/10/04", "2021/10/05",
    "2021/10/06", "2021/10/07", "2021/10/08", "2021/10/09", "2021/10/10",
    "2021/10/11", "2021/10/12",
    ]

date_list = [
    "2021/09/29",
    ]

df_events = get_df_events()
for x in date_list:
    vessel_data_path = "/Users/jufri/Dravam Drive/TS_datavis/KC_AWS/"+x+"/*.csv"
    extract_bunkering_operations(vessel_data_path, df_events)
    