# Low pass filter
from scipy.signal import butter, lfilter, freqz, lfilter_zi

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    # Compute an initial state zi for the lfilter function that corresponds to the steady state of the step response.
    zi = lfilter_zi(b, a)
    y, zf = lfilter(b, a, data, zi=zi*data[0])
    return y

# Filter requirements.
order = 2
fs = 10       # sample rate, Hz
cutoff = 0.05  # desired cutoff frequency of the filter, Hz