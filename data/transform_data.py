from filters.butterworth_filter import butter_lowpass_filter
import pandas as pd

def apply_low_pass_filter(df, order = 2, fs = 10, cutoff = 0.05):
    # for vessel data use this
    # Filter requirements.
    # order = 2
    # fs = 10       # sample rate, Hz
    # cutoff = 0.05  # desired cutoff frequency of the filter, Hz

    df_lpf=pd.DataFrame()

    cols = df.columns
    for col in cols:
        df_lpf[col+"_lpf"] = butter_lowpass_filter(df[col], cutoff, fs, order)
    
    df_lpf.index = df.index

    return df_lpf

def apply_moving_average(df, r_window):
    

    df_mva=pd.DataFrame()

    cols = df.columns
    for col in cols:
        df_mva[col+"_mva"]=df[col].rolling(r_window, min_periods=1).mean()
    
    df_mva.index = df.index

    return df_mva

def apply_normalization(df):
        
    df_norm=pd.DataFrame()

    cols = df.columns
    for col in cols:
        df_norm[col+'_norm'] = (df[col] - df[col].min()) / (df[col].max() - df[col].min())    

    df_norm.index = df.index

    return df_norm