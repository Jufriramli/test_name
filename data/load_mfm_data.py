from data.transform_data import apply_low_pass_filter, apply_moving_average, apply_normalization

import glob
import pandas as pd
# pd.options.plotting.backend = "plotly"
# pd.options.plotting.backend = 'matplotlib'

# mfm data is 1 datapoint every 2 seconds
# rolling window at 5 = 10 secs
def load_mfm_data(path, r_window = 5):

    mfm_col = [
        "Date",
        "Time",
        "Status1",
        "Limit1",
        "Mass Flow",
        "Status2",
        "Limit2",
        "Standard Density"
        ]

    all_filenames = [i for i in glob.glob(path)]
    all_filenames = sorted(all_filenames)
    all_filenames = all_filenames[:]

    # #combine all files in the list
    mfm_comboned_df = pd.concat([pd.read_csv(f, names=mfm_col, sep = ';',skiprows=4) for f in all_filenames ])
    mfm_comboned_df["datetime"] = pd.to_datetime(mfm_comboned_df['Date'] + ' ' + mfm_comboned_df['Time'])
    mfm_comboned_df = mfm_comboned_df[["datetime","Mass Flow", "Standard Density"]]
    mfm_comboned_df = mfm_comboned_df.set_index("datetime")

    df_lpf = apply_low_pass_filter(mfm_comboned_df, order = 2, fs = 0.5, cutoff = 0.05 )
    df_lpf_mva = apply_moving_average(df_lpf, r_window)
    df_lpf_mva_norm = apply_normalization(df_lpf_mva)


    df = pd.concat([mfm_comboned_df, df_lpf, df_lpf_mva, df_lpf_mva_norm],axis=1)

    return df

# path="Mass Flow_Standard Density/*.csv"
# df = load_mfm_data(path)

# # Filter requirements.
# order = 2
# fs = 0.5       # sample rate, Hz
# cutoff = 0.05  # desired cutoff frequency of the filter, Hz