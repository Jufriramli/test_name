# from filters.butterworth_filter import butter_lowpass_filter
from data.transform_data import apply_low_pass_filter, apply_moving_average, apply_normalization

import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def load_csv_from_path(path):

    header_list = [
        "time", "date_year", "date_month", "date_day", 
        "time_hr", "time_min", "time_sec", "time_Msec", 
        "pt_0001_load_value", "tt_0001_load_value", # pressure, temp
        "dp_0001_load_value", "dp_0001_unload_value", # diff pressure low 0-2000, high 0-5000
        "dp_0002_load_value", "dp_0002_unload_value",
        ]

    variable_list=[
        "pt_0001_load_value", "tt_0001_load_value", 
        "dp_0001_load_value", "dp_0001_unload_value",
        "dp_0002_load_value", "dp_0002_unload_value"
        ]

    all_filenames = [i for i in glob.glob(path)]
    all_filenames = sorted(all_filenames)
    all_filenames = all_filenames[:]
    # for fname in glob.glob(path):
    #    print(fname)

    # #combine all files in the list
    df_day_combined = pd.concat([pd.read_csv(f, names=header_list) for f in all_filenames ])

    df_day_combined=df_day_combined[["time"]+variable_list]

    df_day_combined['time'] = pd.to_datetime(df_day_combined['time'], format='%Y-%m-%d %H:%M:%S.%f')

    # sort combined data by time
    df_day_combined=df_day_combined.sort_values(by='time', ascending=True)
    df_day_combined = df_day_combined.set_index("time")

    return df_day_combined

# vessel data is 10 datapoint every 1 seconds
# rolling window at 100 = 10 secs
def load_vessel_data(path, r_window = 100):
    """Loads a day worth of data from folder path

    Args:
        path (str): path = r"/Users/jufri/Dravam Drive/TS_datavis/KC_AWS/2021/09/05/*.csv"
        r_window (int, optional): Rolling window for moving average on low pass data. Defaults to 1200.

    Returns:
        df: dataframe of original, lpf and mva
    """
    # to load data change the path below
    df = load_csv_from_path(path)
    df_lpf = apply_low_pass_filter(df)
    df_lpf_mva = apply_moving_average(df_lpf, r_window)
    df_lpf_mva_norm = apply_normalization(df_lpf_mva)

    df = pd.concat([df, df_lpf, df_lpf_mva, df_lpf_mva_norm],axis=1)
    return df


# path = r"/Users/jufri/Dravam Drive/TS_datavis/KC_AWS/2021/09/04/*.csv"
# df = load_vessel_data(path)
