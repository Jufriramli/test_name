# from filters.butterworth_filter import butter_lowpass_filter
from data.transform_data import apply_low_pass_filter, apply_moving_average, apply_normalization

import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def load_csv_from_path(path):

    operation = path.split("_")[-1].replace(".csv","")

    variable_list=[
        "pt_0001_load_value", "tt_0001_load_value", 
        "dp_0001_load_value", "dp_0001_unload_value",
        "dp_0002_load_value", "dp_0002_unload_value"
        ]

    all_filenames = [i for i in glob.glob(path)]
    all_filenames = sorted(all_filenames)
    all_filenames = all_filenames[:]
    # for fname in glob.glob(path):
    #    print(fname)

    # #combine all files in the list
    operation_df = pd.concat([pd.read_csv(f, names=["time"]+variable_list, header=0,  index_col=False) for f in all_filenames ])

    operation_df=operation_df[["time"]+variable_list]

    operation_df['time'] = pd.to_datetime(operation_df['time'], format='%Y-%m-%d %H:%M:%S.%f')

    # sort combined data by time
    operation_df=operation_df.sort_values(by='time', ascending=True)
    operation_df = operation_df.set_index("time")

    return operation_df

# vessel data is 10 datapoint every 1 seconds
# rolling window at 100 = 10 secs
def load_operation_data(path, r_window = 100):
    # to load data change the path below
    df = load_csv_from_path(path)
    df_lpf = apply_low_pass_filter(df)
    df_lpf_mva = apply_moving_average(df_lpf, r_window)
    df_lpf_mva_norm = apply_normalization(df_lpf_mva)

    df = pd.concat([df, df_lpf, df_lpf_mva, df_lpf_mva_norm],axis=1)
    return df


# path = "/Users/jufri/SynologyDrive/files_exports/exported_events_raw_data/Unloading/2021-09-27 10_23_13.185157_Unloading.csv"
# df = load_operation_data(path)
