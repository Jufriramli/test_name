# Running this will show how many records are there for each day
from data.load_vessel_data import load_vessel_data

import glob
import pandas as pd
import numpy as np
import datetime
import seaborn as sns
import matplotlib.pyplot as plt

def get_df_stat(date_list):
    overall_stats=pd.DataFrame()
    for date in date_list:
        path = "/Users/jufri/Dravam Drive/TS_datavis/KC_AWS/{}/*.csv".format(date)
        df_day_combined = load_vessel_data(path)

        day_stats = df_day_combined.describe()
        day_stats["date"]=date

        overall_stats = pd.concat([overall_stats, day_stats])

    return overall_stats

date_list = [
    # "2021/09/01", "2021/09/02", "2021/09/03", "2021/09/04", "2021/09/05",
    # "2021/09/06", "2021/09/07", "2021/09/08", "2021/09/09", "2021/09/10",
    # "2021/09/11", "2021/09/12", "2021/09/13", "2021/09/14", "2021/09/15",
    # "2021/09/16", "2021/09/17", "2021/09/18", "2021/09/19", "2021/09/20",
    # "2021/09/21", "2021/09/22", "2021/09/23", "2021/09/24", "2021/09/25",
    # "2021/09/26",
    # "2021/09/27", "2021/09/28", "2021/09/29", "2021/09/30",
    # "2021/10/01", "2021/10/02", "2021/10/03", "2021/10/04", "2021/10/05",
    # "2021/10/06", "2021/10/07", "2021/10/08", "2021/10/09", "2021/10/10",
    "2021/10/11", "2021/10/12",
    ]

overall_stats = get_df_stat(date_list)
daily_count = overall_stats[overall_stats.index=="count"][["date","tt_0001_load_value"]].copy()
daily_count["datediff"]=daily_count["tt_0001_load_value"].apply(lambda x: 864001-x)
daily_count

# %%
# To pull out individual stats
# stats_list = ["count", "mean", "std", "min", "25%", "50%", "75%", "max"]

# for s in ["min", "max"]: #stats_list:
#     for i, col in enumerate(overall_stats[overall_stats.index==s].columns):
#         if col!="date":
#             overall_stats[overall_stats.index==s][["date",col]].plot(x="date", fig=plt.figure(i), rot=90, figsize=(15,10))
#             plt.title(col+"_"+s)

#     plt.show()

# %%
## Plot graph of difference
# path = "/Users/jufri/Dravam Drive/TS_datavis/KC_AWS/2021/09/12/*.csv"
# df_day_combined = get_df_day_combined(path)
# df_day_combined["datediff"]=df_day_combined["time"].diff()
# df_day_combined[["time","datediff"]].plot(x="time", figsize=(15,5))