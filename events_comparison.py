from data.load_operation_data import load_operation_data

import pandas as pd
import numpy as np
import glob

path = 'exported_events_raw_data/Loading/2021-10-11 10_03_14.874820_Loading.csv'
path_list = [
    # 'exported_events_raw_data/Loading/2021-10-11 10_03_14.874820_Loading.csv',
    # 'exported_events_raw_data/Loading/2021-10-07 11_14_04.870383_Loading.csv',
    # 'exported_events_raw_data/Loading/2021-10-04 11_58_01.272405_Loading.csv',
    # 'exported_events_raw_data/Loading/2021-10-02 08_09_42.384095_Loading.csv',
    # 'exported_events_raw_data/Loading/2021-09-29 15_42_58.285266_Loading.csv',
    "/Users/jufri/SynologyDrive/files_exports/exported_events_raw_data/temp/2021-09-30 08_12_27.274570_Unloading.csv",
    "/Users/jufri/SynologyDrive/files_exports/exported_events_raw_data/Unloading/2021-09-27 10_23_13.185157_Unloading.csv",
    "/Users/jufri/SynologyDrive/files_exports/exported_events_raw_data/Unloading/2021-09-08 05_56_29.581773_Unloading.csv",
    
]

operation_file_df = pd.DataFrame()
operation_df = pd.DataFrame()

variable_list=[
    "pt_0001_load_value", "tt_0001_load_value", 
    "dp_0001_load_value", "dp_0001_unload_value",
    "dp_0002_load_value", "dp_0002_unload_value"
    ]
for idx, lpath in enumerate(path_list):
    operation_file_df = load_operation_data(lpath)
    operation_file_df = operation_file_df[:18000].reset_index()
    operation_df["col"+str(idx)]=operation_file_df["dp_0001_unload_value_lpf_mva_norm"]
    operation_df["col"+str(idx)]=operation_df["col"+str(idx)].rolling(100, min_periods=1).mean()

operation_df.plot(figsize=(15,10), alpha=0.6)
